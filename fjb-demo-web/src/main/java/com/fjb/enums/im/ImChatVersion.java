package com.fjb.enums.im;

/**
 * @Description:聊天信息版本	 1.0.0 开始  ,
 * @author hemiao
 * @time:2020年1月4日 下午8:27:38
 */
public enum ImChatVersion {
	
	/**
	 *  聊天信息版本	1.0.0
	 */
	VSERSION("1.0.0","版本1.0.0");
	
	private String code;
    private String msg;
	
	private ImChatVersion(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
	
    public static String getMsgName(String code) {
    	if(code==null) {
    		return null;
    	}
        for (ImChatVersion o : ImChatVersion.values()) {
	        if (o.getCode().equals(code)) {
	            return o.msg;
	        }
        }
        return null;
    }

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
