package com.fjb.enums.im;

/**
 * @Description:数据来源：  1000、电脑网页 （默认）
 * @author hemiao
 * @time:2020年1月4日 下午8:27:38
 */
public enum ImDataSources {
	
	/**
	 *   1000、电脑网页 （默认）
	 */
	S_1000(1000,"电脑网页");
	
	private Integer code;
    private String msg;
	
	private ImDataSources(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }
	
    public static String getMsgName(Integer code) {
    	if(code==null) {
    		return null;
    	}
        for (ImDataSources o : ImDataSources.values()) {
	        if (o.getCode().equals(code)) {
	            return o.msg;
	        }
        }
        return null;
    }

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
