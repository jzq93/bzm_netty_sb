package com.fjb.entity;

/**
 * @Description:TODO
 * @author hemiao
 * @time:2020年1月31日 下午3:25:13
 */
public class TotalResult<T> {
	
	/**
	 * 返回数据
	 */
    private T dataObject;
	
	/**
     * 总数量
     */
    private Long total;
    
    /**
     * 总页数
     */
    private int pages;
    
    
    public TotalResult(T dataObject,Long total,int pages) {
        this.dataObject = dataObject;
        this.total=total;
        this.pages=pages;
    }

	public T getDataObject() {
		return dataObject;
	}

	public void setDataObject(T dataObject) {
		this.dataObject = dataObject;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}
}
