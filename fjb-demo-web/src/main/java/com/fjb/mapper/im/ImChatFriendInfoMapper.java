package com.fjb.mapper.im;

import com.fjb.pojo.im.ImChatFriendInfo;
import com.fjb.pojo.im.vo.ImChatFriendInfoVo;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * im_chat 朋友信息表 Mapper 接口
 * </p>
 *
 * @author hemiao
 * @since 2020-06-02
 */
public interface ImChatFriendInfoMapper extends BaseMapper<ImChatFriendInfo> {
	
	/**
	 * @Description:查询好友列表
	 * @param userId
	 * @return
	 * List<ImChatFriendInfoVo>
	 * @exception:
	 * @author: hemiao
	 * @time:2020年6月3日 下午1:11:35
	 */
	List<ImChatFriendInfoVo> selectFriendList(@Param("userId")Integer userId);

}
