package com.fjb.tool.thread;

import java.util.concurrent.BlockingQueue;

/**
 * @Description:中断线程 
 * @author hemiao
 * @time:2020年4月20日 下午5:03:09
 */
public class InterruptedRunnable {
	
	BlockingQueue sss;
	
	public static void main(String[] args) {
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					
				} catch (Exception e) {
					// 异常发生终止线程  业务代码有异常必须抛出来  这里处理  否则线程不会终止
					Thread.currentThread().interrupt();
				}
			}
		}).start();
		
		
		
	}
	
}
