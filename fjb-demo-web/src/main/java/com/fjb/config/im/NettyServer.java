package com.fjb.config.im;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * @Description:netty 服务
 * @author hemiao
 * @time:2020年5月20日 下午9:14:15
 */
@Component
public class NettyServer {
	
	private static final Logger log = LoggerFactory.getLogger(NettyServer.class);
	
	/**
	 *  netty端口
	 */
	private static int port = 8080;
	
	private static class SingletionWSServer {
		static final NettyServer instance = new NettyServer();
	}
	
	public static NettyServer getInstance() {
		return SingletionWSServer.instance;
	}
	
	private EventLoopGroup mainGroup;
	private EventLoopGroup subGroup;
	private ServerBootstrap server;
	private ChannelFuture future;
	
	public NettyServer() {
		mainGroup = new NioEventLoopGroup();
		subGroup = new NioEventLoopGroup();
		server = new ServerBootstrap();
		server.group(mainGroup, subGroup)
			.channel(NioServerSocketChannel.class)
			.childHandler(new NettyChannelInitializer());
	}
	
	public void start() {
		this.future = server.bind(port);
		log.info("netty server server 启动完毕... port = "+port);
	}
	
}
