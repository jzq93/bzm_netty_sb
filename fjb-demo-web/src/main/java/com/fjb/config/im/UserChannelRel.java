package com.fjb.config.im;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.netty.channel.Channel;

/**
 * @Description:用户id和channel的关联关系处理
 * @author hemiao
 * @time:2020年5月21日 下午10:57:30
 */
public class UserChannelRel {
	
	private static final Logger log = LoggerFactory.getLogger(UserChannelRel.class);
		
	private static HashMap<String, Channel> manager = new HashMap<>();

	public static void put(String senderId, Channel channel) {
		manager.put(senderId, channel);
	}
	
	public static Channel get(String senderId) {
		return manager.get(senderId);
	}
		
	public static void output() {
		for (HashMap.Entry<String, Channel> entry : manager.entrySet()) {
			log.info(" imChat获得连接：  UserId: " + entry.getKey() 
							+ ", ChannelId: " + entry.getValue().id().asLongText());
		}
	}
	
}
